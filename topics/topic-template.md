# Research Project Template

### Working Groups: aa, cm, dm, nm, st

### Collaborators (MAT): ataraz, cseifert, dclemens, dgallaun, druprecht, fboesch, fbuenger, fgabel, fhamann, hruan, hvoss, jangel, jdornemann, jfregin, jgrams, jgrossmann, jmeichsner, jpmzemke, jurizarna, kalbrecht, kklioba, kkruse, mjanssen, mlindner, mschulte, mwolkner, pbaasch, pgupta, rbeddig, rukena, sgoetschel, sleborne, sotten, tsaathoff, vgriem, vtrapp, wleinen, wmackens, ymogge

### Collaborators (External): [Vaughan Jones](https://as.vanderbilt.edu/math/bio/?who=vaughan-jones), [Terry Tao](https://terrytao.wordpress.com/about/)

## Description

This is about math and contains some nice inline formulas like $\int_0^1 \sin(x) \mathrm{d}x$ and also some displayed formulas like

$$
\int_0^1 \sin(x) \mathrm{d}x
$$

It also contains some nice pictures


![Our Institute Logo](/img/logo_header_mat_de.png)


## References

[1] Integrability Methods in Harmonic Galois Theory. F. Gabel, M. Chebyshev, Z. B. Hadamard and N. Deligne. Available at [https://thatsmathematics.com/mathgen/](https://thatsmathematics.com/mathgen/paper.php?nameType%5B1%5D=custom&customName%5B1%5D=F.+Gabel&nameType%5B2%5D=famous&nameType%5B3%5D=famous&nameType%5B4%5D=famous&seed=126904073&format=pdf
)
