# Fractional Powers of Linear operators

### Working Groups: aa

### Collaborators (MAT): jmeichsner, cseifert, dgallaun,  kkruse

## Description

(**DRAFT**)

This research topic deals with so-called fractional powers of linear operators in Banach or even more general locally convex spaces. 
Roughly it is about defining and studying operators of the form $A^{\alpha}$ for a given 'base operator' $A$, a typically closed but discontinuous linear operator with some further properties, and a complex power $\alpha \in \mathbb{C}$. 

Such fractional operators show up at various occasions in mathematics. To mention a concrete instance, many constructions in the realm of fractional calculus, that is fractional derivatives and integrals, can be understood as applications of the abstract operator theoretic setting to concrete differential and integral operators. 
Another example is the Caffarelli-Silvestre or harmonic extension in which fractional powers correspond to some kind of generalised Dirichlet-to-Neumann Operator for an ODE in a Banach space. 

A possible way to tackle them is subordination, an instance of a general functional calculus called Hille-Phillips calculus for the generators of $C_0$-Semigroups. 

## References

### Papers

[1] J. Meichsner, C. Seifert. On the harmonic extension approach to fractional powers in Banach spaces. *Fract. Calc. Appl. Anal.*, 23(4):1054–1089, 2020.

### Proceedings

[1] J. Meichsner, C. Seifert. On some Consequences of the Solvability of the Caffarelli–Silvestre Extension Problem. In M. A. Bastos, L. Castro, and A. Yu. Karlovich, editors, *Operator Theory, Functional Analysis and Applications*, volume 282 of *Operator Theory: Advances and Applications*, 441-453, Birkhäuser Basel, 2021.

[2] J. Meichsner, C. Seifert. A Note on the Harmonic Extension Approach to Fractional Powers of non-densely defined Operators. In J. Eberhardsteiner and M. Schöberl, editors, *Proceedings in Applied Mathematics and Mechanics*, volume 19, 1–2, 2019.

### Preprints

[1] K. Kruse, J. Meichsner, C. Seifert. Subordination for sequentially equicontinuous equibounded
C0–semigroups, 2018. Arxiv preprint https://arxiv.org/pdf/1802.05059.pdf.
submitted

[2] J. Meichsner, C. Seifert. Fractional powers of non-negative operators in Banach spaces via the Dirichlet-to-Neumann operator, 2017. Arxiv preprint (v3)
https://arxiv.org/pdf/1704.01876.pdf.