  <style>
    html {
      line-height: 1.5;
      font-family: Georgia, serif;
      font-size: 20px;
      color: #1a1a1a;
      background-color: #fdfdfd;
    }
    body {
      margin: 0 auto;
      max-width: 36em;
      padding-left: 50px;
      padding-right: 50px;
      padding-top: 50px;
      padding-bottom: 50px;
      hyphens: auto;
      word-wrap: break-word;
      text-rendering: optimizeLegibility;
      font-kerning: normal;
    }
    @media (max-width: 600px) {
      body {
        font-size: 0.9em;
        padding: 1em;
      }
    }
    @media print {
      body {
        background-color: transparent;
        color: black;
        font-size: 12pt;
      }
      p, h2, h3 {
        orphans: 3;
        widows: 3;
      }
      h2, h3, h4 {
        page-break-after: avoid;
      }
    }
    p {
      margin: 1em 0;
    }
    a {
      color: #1a1a1a;
    }
    a:visited {
      color: #1a1a1a;
    }
    img {
      max-width: 100%;
    }
    h1, h2, h3, h4, h5, h6 {
      margin-top: 1.4em;
    }
    h5, h6 {
      font-size: 1em;
      font-style: italic;
    }
    h6 {
      font-weight: normal;
    }
    ol, ul {
      padding-left: 1.7em;
      margin-top: 1em;
    }
    li > ol, li > ul {
      margin-top: 0;
    }
    blockquote {
      margin: 1em 0 1em 1.7em;
      padding-left: 1em;
      border-left: 2px solid #e6e6e6;
      color: #606060;
    }
    code {
      font-family: Menlo, Monaco, 'Lucida Console', Consolas, monospace;
      font-size: 85%;
      margin: 0;
    }
    pre {
      margin: 1em 0;
      overflow: auto;
    }
    pre code {
      padding: 0;
      overflow: visible;
    }
    .sourceCode {
     background-color: transparent;
     overflow: visible;
    }
    hr {
      background-color: #1a1a1a;
      border: none;
      height: 1px;
      margin: 1em 0;
    }
    table {
      margin: 1em 0;
      border-collapse: collapse;
      width: 100%;
      overflow-x: auto;
      display: block;
      font-variant-numeric: lining-nums tabular-nums;
    }
    table caption {
      margin-bottom: 0.75em;
    }
    tbody {
      margin-top: 0.5em;
      border-top: 1px solid #1a1a1a;
      border-bottom: 1px solid #1a1a1a;
    }
    th {
      border-top: 1px solid #1a1a1a;
      padding: 0.25em 0.5em 0.25em 0.5em;
    }
    td {
      padding: 0.125em 0.5em 0.25em 0.5em;
    }
    header {
      margin-bottom: 4em;
      text-align: center;
    }
    #TOC li {
      list-style: none;
    }
    #TOC a:not(:hover) {
      text-decoration: none;
    }
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="stokes-operator-on-lipschitz-domains">Stokes Operator on Lipschitz Domains</h1>
<h3 id="working-groups-lehrstuhl-angewandte-analysis">Working Groups: <a href="aa.html">Lehrstuhl Angewandte Analysis</a></h3>
<h3 id="collaborators-mat-fabian-gabel-m.-sc.">Collaborators (MAT): <a href="fgabel.html">Fabian Gabel, M. Sc.</a></h3>
<h3 id="collaborators-external-patrick-tolksdorf">Collaborators (External): <a href="https://www.funktionalanalysis.mathematik.uni-mainz.de/patrick-tolksdorf/">Patrick Tolksdorf</a></h3>
<h2 id="description">Description</h2>
<p>In the solution theory for nonlinear partial differential equations, an integral part of the solution process is often to develop a semigroup theory for the linearization of the equation. In the case of the famous <em>Navier-Stokes equations</em> which for a given domain <span class="math inline">\(\Omega \subseteq \mathbb{R}^d\)</span>, <span class="math inline">\(d \geq 2\)</span>, describe the behavior of a Newtonian fluid over time, the linearization is given by the <em>Stokes equations</em></p>
<p><span class="math display">\[
  \partial_t u - \Delta u + \nabla \pi = 0 \quad\text{in } \Omega\,, \;t &gt; 0\,, \quad
  \operatorname{div}(u) = 0 \quad\text{in } \Omega\,,\; t &gt; 0\,,  
\]</span></p>
<p><span class="math display">\[
  u(0) = a \text{ in } \Omega\,, 
  u = 0 \text{ on } \partial\Omega\,,\; t &gt; 0\,,
\]</span></p>
<p>where <span class="math inline">\(u \colon \mathbb{R}^+ \times \Omega \to \mathbb{R}^d\)</span> stands for the velocity field and <span class="math inline">\(\pi \colon \mathbb{R}^+ \times \Omega \to \mathbb{R}\)</span> represents the pressure of the fluid. The so-called <em>Stokes semigroup</em> <span class="math inline">\((\mathrm{e}^{-tA})_{t \geq 0}\)</span> describes the evolution of the velocity <span class="math inline">\(u\)</span> and the <em>Stokes operator</em> <span class="math inline">\(A\)</span> corresponds to the term ’‘<span class="math inline">\(-\Delta u + \nabla \pi\)</span>’’ in the Stokes equations.</p>
<p>Having a semigroup makes it possible to look for <em>mild solutions</em> to the Navier-Stokes equations using a variation of constants formula to construct an iteration method. This approach was introduced by Fujita and Kato [1] and builds mainly on resolvent estimates for the Stokes operator <span class="math inline">\(A\)</span> and the analyticity property of the Stokes semigroup.</p>
<h2 id="references">References</h2>
<p>[1] Fujita, H. and Kato, T. On the Navier-Stokes initial value problem I. Archive for Rational Mechanics and Analysis 16(1964), 269–315.</p>
<p>[2] Tolksdorf, P. On the Lp-theory of the Navier-Stokes equations on Lipschitz domains. PhD thesis, Technische Universität Darmstadt, 2017. Available at http://tuprints.ulb.tu-darmstadt.de/5960/.</p>
<p>[3] Gabel, F. On Resolvent Estimates in Lp for the Stokes Operator in Lipschitz Domains. Master thesis, Technische Universität Darmstadt, 2018.</p>
<p>[4] Gabel, F. and Tolksdorf, P. The Stokes operator in two-dimensional bounded Lipschitz domains. <em>In preparation.</em></p>
</body>
</html>
