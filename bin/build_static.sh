#!/bin/bash
# build the static webpage locally
BUILD_DIR=$RESEARCH_ROOT/static/build
DEPLOY_DIR=$RESEARCH_ROOT/static
BIN_DIR=$RESEARCH_ROOT/bin

# clean up directories
rm -rf  $BUILD_DIR
rm -rf $DEPLOY_DIR/{.public,public}

#build html 
echo "Preproc Topics"
$BIN_DIR/preproc_topics.sh $RESEARCH_ROOT/static
echo "Run Pandoc"
$BIN_DIR/integrate_pandoc.sh
echo "Pandoc finished."

#make public directory and copy files
mkdir -p $DEPLOY_DIR/.public
for f in $BUILD_DIR/*_final.html
do
    filename=$(basename -- "$f")
    cp $f $DEPLOY_DIR/.public/${filename%_final.html}.html
done
cp -r $RESEARCH_ROOT/img $DEPLOY_DIR/.public
cp -r $RESEARCH_ROOT/css $DEPLOY_DIR/.public
mv $DEPLOY_DIR/.public $DEPLOY_DIR/public

echo "Done. Visit file://$DEPLOY_DIR/public/index.html ."
