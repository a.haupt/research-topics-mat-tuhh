#!/bin/bash

for f in *.html
do 
  sed -i 's;windows-1252;utf-8;g' $f
  sed -i 's;iso-8859-1;utf-8;g' $f
  sed -i 's;/include/css;css;g' $f 
done
