#!/bin/bash
# preproc_staff.sh
# Preprocessor for staff keys in .md topic file
# Replaces key fgabel by "Fabian Gabel, M.Sc." according to Webpage content
# If Build_dir is passed, titles are appended to corresponding files
# Note: mat.tuhh.de uses WINDOWS-1252 character encoding

# processing command line arguments
if [ $# -lt 2 ]
then
    RESEARCH_BUILD=`pwd`"/build"
    echo -e "No building directory was specified."
else
    RESEARCH_BUILD="$2"
fi
mkdir -p $RESEARCH_BUILD
echo -e "Building into directory $RESEARCH_BUILD ..."

filename=$(basename -- "$1")
baseurl='https://www.mat.tuhh.de'
#make output copy
echo "Preprocessing $filename..."
if [ -f "$RESEARCH_BUILD/$filename" ]
then
    echo "File exists, performing preprocessing in place"
else
    cp $1 $RESEARCH_BUILD/$filename
fi

#extract title of topic
echo "Extracting title of $1..."
title=`$RESEARCH_ROOT/bin/extract_title.sh $RESEARCH_BUILD/$filename`
echo "Title of Topic: $title"

# start preprocessing 
# -- leave original untouched, only work with copy in $RESEARCH_BUILD
# replace working group
echo "Replacing working group keys..."
namelist=`grep -h -i -m 1 -r "###\s*Working Groups:" $RESEARCH_BUILD/$filename | sed -e 's/^###\s*Working Groups:\s*//I' -e 's/\s*,\s*/\n/g'  | sort -u`
echo "Found the following keys: " $namelist
for wg in $namelist
do
    #extract full name from tuhh-webpage
    fullname=`wget -qO- $baseurl/forschung/$wg | grep h1 | sed -e "s;<h1>\s*\(.*\)</h1>.*$;\1;g" | sed -e 's;^[ \t]*;;'`

    echo "Found working group $fullname"

    # append research to wg-file
    if [ $# -gt 1 ]
    then
    echo -e "\n$title\n" >> $RESEARCH_BUILD/$wg.md
    fi

    sed -i "s;### Working Groups:\(.*\)$wg\(.*\);### Working Groups:\1\[$fullname\]($wg.html)\2;g" $RESEARCH_BUILD/$filename  
    #sed -i "s;### Working Groups:\(.*\)$wg\(.*\);### Working Groups:\1\[$fullname\]($baseurl/forschung/$wg)\2;g" $RESEARCH_BUILD/$filename  #uncomment this line for linking the mat.tuhh.de webpage
done