#!/bin/bash
# extract title of research topic

filename=$(basename -- "$1")

echo "## `head -n 1 $1 | sed -e "s;#\s*;\[;g" -e "s;\(.*\);\1\](${filename%.md}.html);g"`"